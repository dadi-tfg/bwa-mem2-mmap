/*************************************************************************************
                           The MIT License

   BWA-MEM2  (Sequence alignment using Burrows-Wheeler Transform),
   Copyright (C) 2019  Vasimuddin Md, Sanchit Misra, Intel Corporation, Heng Li.

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

Authors: Sanchit Misra <sanchit.misra@intel.com>; Vasimuddin Md <vasimuddin.md@intel.com>;
*****************************************************************************************/

#include <stdio.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "sais.h"
#include "FMI_search.h"

FMI_search::FMI_search(const char *fname)
{
    fprintf(stderr, "* Entering FMI_search\n");
    strcpy(file_name, fname);
    sa_ls_word = NULL;
    sa_ms_byte = NULL;
    cp_occ = NULL;
#if ((__AVX2__))
    c_bcast_array = NULL;
#endif
}

FMI_search::~FMI_search()
{
    if(sa_ms_byte)
        _mm_free(sa_ms_byte);
    if(sa_ls_word)
        _mm_free(sa_ls_word);
    if(cp_occ)
        _mm_free(cp_occ);
#if ((__AVX2__))
    if(c_bcast_array)
        _mm_free(c_bcast_array);
#endif
}

int64_t FMI_search::pac_seq_len(const char *fn_pac)
{
	FILE *fp;
	int64_t pac_len;
	uint8_t c;
	fp = xopen(fn_pac, "rb");
	err_fseek(fp, -1, SEEK_END);
	pac_len = err_ftell(fp);
	err_fread_noeof(&c, 1, 1, fp);
	err_fclose(fp);
	return (pac_len - 1) * 4 + (int)c;
}

void FMI_search::pac2nt(const char *fn_pac, std::string &reference_seq)
{
	uint8_t *buf2;
	int64_t i, pac_size, seq_len;
	FILE *fp;

	// initialization
	seq_len = pac_seq_len(fn_pac);
	fp = xopen(fn_pac, "rb");

	// prepare sequence
	pac_size = (seq_len>>2) + ((seq_len&3) == 0? 0 : 1);
	buf2 = (uint8_t*)calloc(pac_size, 1);
  // buf2 =  (uint8_t*)mmap(NULL, pac_size, PROT_READ, MAP_SHARED|MAP_POPULATE, fileno(fp), 0);
	err_fread_noeof(buf2, 1, pac_size, fp);
	err_fclose(fp);
	for (i = 0; i < seq_len; ++i) {
		int nt = buf2[i>>2] >> ((3 - (i&3)) << 1) & 3;
        switch(nt)
        {
            case 0:
                reference_seq += "A";
            break;
            case 1:
                reference_seq += "C";
            break;
            case 2:
                reference_seq += "G";
            break;
            case 3:
                reference_seq += "T";
            break;
            default:
                fprintf(stderr, "ERROR! Value of nt is not in 0,1,2,3!");
                exit(EXIT_FAILURE);
        }
	}
    for(i = seq_len - 1; i >= 0; i--)
    {
        char c = reference_seq[i];
        switch(c)
        {
            case 'A':
                reference_seq += "T";
            break;
            case 'C':
                reference_seq += "G";
            break;
            case 'G':
                reference_seq += "C";
            break;
            case 'T':
                reference_seq += "A";
            break;
        }
    }
	free(buf2);
}

int64_t FMI_search::pac2nt_mmap(const char *fn_pac, char **output_reference_seq)
{
	uint8_t *buf2;
	int64_t i, pac_size, seq_len;

  // TODO: DESCUBRIR EL TAM. DEL REF SEQ
  int64_t size;
	FILE *fp;

	// initialization
	seq_len = pac_seq_len(fn_pac);
	fp = xopen(fn_pac, "rb");

	// prepare sequence
	pac_size = (seq_len>>2) + ((seq_len&3) == 0? 0 : 1);
	// buf2 = (uint8_t*)calloc(pac_size, 1);
  buf2 =  (uint8_t*)mmap(NULL, pac_size, PROT_READ, MAP_SHARED, fileno(fp), 0);
	// err_fread_noeof(buf2, 1, pac_size, fp);
	err_fclose(fp);

  // create aux reference sequence file and map it
  int fd = open("aux_ref_seq", O_CREAT|O_RDWR, 0644);
  ftruncate(fd, ((2*seq_len) +1) *sizeof(char));
  printf("Created file aux_ref_seq with size %d\n", 2*seq_len);
  *output_reference_seq = (char *)mmap(NULL, ((2*seq_len) +1) *sizeof(char), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
  char * reference_seq = *output_reference_seq;
  // printf("Mapeado ref seq en %p\n", (void*)(reference_seq));
  close(fd);

  // printf("Antes del for largo del seqlen\n");

  char *iterator = &reference_seq[0];
  int nt;
	for (i = 0; i < seq_len; ++i) {
		nt = buf2[i>>2] >> ((3 - (i&3)) << 1) & 3;
        switch(nt)
        {
            case 0:
                // reference_seq += "A";
                iterator[0] = 'A';
                iterator++;
            break;
            case 1:
                // reference_seq += "C";
                iterator[0] = 'C';
                iterator++;
            break;
            case 2:
                // reference_seq += "G";
                iterator[0] = 'G';
                iterator++;
            break;
            case 3:
                // reference_seq += "T";
                iterator[0] = 'T';
                iterator++;
            break;
            default:
                fprintf(stderr, "ERROR! Value of nt is not in 0,1,2,3!");
                exit(EXIT_FAILURE);
        }
	}
  // printf("Antes del segundo for largo del seqlen\n");

  char c;
    for(i = seq_len - 1; i >= 0; i--)
    {
      // printf("%d\n", i);
        // char c = reference_seq[i];
        c = reference_seq[i];
        switch(c)
        {
            case 'A':
                // reference_seq += "T";
                iterator[0] = 'T';
                iterator++;
            break;
            case 'C':
                // reference_seq += "G";
                iterator[0] = 'G';
                iterator++;
            break;
            case 'G':
                // reference_seq += "C";
                iterator[0] = 'C';
                iterator++;
            break;
            case 'T':
                // reference_seq += "A";
                iterator[0] = 'A';
                iterator++;
            break;
        }
    }
    // printf("Despues de los for largos del seqlen\n");

	// free(buf2);
  munmap(buf2, pac_size);
  return ((2* seq_len)+1);
}

int FMI_search::build_fm_index_avx(const char *ref_file_name, char *binary_seq, int64_t ref_seq_len, int64_t *sa_bwt, int64_t *count) {
    printf("ref_seq_len = %ld\n", ref_seq_len);
    fflush(stdout);

    char outname[200];
    sprintf(outname, "%s.bwt.8bit.%d", ref_file_name, CP_BLOCK_SIZE_AVX);

    std::fstream outstream (outname, std::ios::out | std::ios::binary);
    outstream.seekg(0);

    printf("count = %ld, %ld, %ld, %ld, %ld\n", count[0], count[1], count[2], count[3], count[4]);
    fflush(stdout);

    uint8_t *bwt;

    ref_seq_len++;
    outstream.write((char *)(&ref_seq_len), 1 * sizeof(int64_t));
    outstream.write((char*)count, 5 * sizeof(int64_t));

    int64_t i;
    int64_t ref_seq_len_aligned = ((ref_seq_len + CP_BLOCK_SIZE_AVX - 1) / CP_BLOCK_SIZE_AVX) * CP_BLOCK_SIZE_AVX;
    int64_t size = ref_seq_len_aligned * sizeof(uint8_t);
    bwt = (uint8_t *)_mm_malloc(size, 64);
    index_alloc += size;
    assert_not_null(bwt, size, index_alloc);

    for(i=0; i< ref_seq_len; i++)
    {
        if(sa_bwt[i] == 0)
        {
            bwt[i] = 4;
            printf("BWT[%ld] = 4\n", i);
        }
        else
        {
            char c = binary_seq[sa_bwt[i]-1];
            switch(c)
            {
                case 0: bwt[i] = 0;
                          break;
                case 1: bwt[i] = 1;
                          break;
                case 2: bwt[i] = 2;
                          break;
                case 3: bwt[i] = 3;
                          break;
                default:
                        fprintf(stderr, "ERROR! i = %ld, c = %c\n", i, c);
                        exit(EXIT_FAILURE);
            }
        }
    }
    for(i = ref_seq_len; i < ref_seq_len_aligned; i++)
        bwt[i] = DUMMY_CHAR;


    printf("CP_SHIFT = %d, CP_MASK = %d\n", CP_SHIFT_AVX, CP_MASK_AVX);
    printf("sizeof CP_OCC = %ld\n", sizeof(CP_OCC_AVX));
    fflush(stdout);
    // create checkpointed occ
    int64_t cp_occ_size = (ref_seq_len >> CP_SHIFT_AVX) + 1;
    CP_OCC_AVX *cp_occ = NULL;

    size = cp_occ_size * sizeof(CP_OCC_AVX);
    cp_occ = (CP_OCC_AVX *)_mm_malloc(size, 64);
    index_alloc += size;
    assert_not_null(cp_occ, size, index_alloc);
    memset(cp_occ, 0, cp_occ_size * sizeof(CP_OCC_AVX));
    int64_t cp_count[16];

    memset(cp_count, 0, 16 * sizeof(int64_t));
    for(i = 0; i < ref_seq_len; i++)
    {
        if((i & CP_MASK_AVX) == 0)
        {
            CP_OCC_AVX cpo;
            cpo.cp_count[0] = cp_count[0];
            cpo.cp_count[1] = cp_count[1];
            cpo.cp_count[2] = cp_count[2];
            cpo.cp_count[3] = cp_count[3];
			memcpy(cpo.bwt_str, bwt + i, CP_BLOCK_SIZE_AVX * sizeof(uint8_t));

            cp_occ[i >> CP_SHIFT_AVX] = cpo;
        }
        cp_count[bwt[i]]++;
    }
    outstream.write((char*)cp_occ, cp_occ_size * sizeof(CP_OCC_AVX));
    _mm_free(cp_occ);
    _mm_free(bwt);
    index_alloc -= (ref_seq_len_aligned * sizeof(uint8_t) + cp_occ_size * sizeof(CP_OCC_AVX));

    size = ref_seq_len * sizeof(uint32_t);
    uint32_t *sa_ls_word = (uint32_t *)_mm_malloc(size, 64);
    index_alloc += size;
    assert_not_null(sa_ls_word, size, index_alloc);
    size = ref_seq_len * sizeof(int8_t);
    int8_t *sa_ms_byte = (int8_t *)_mm_malloc(size, 64);
    index_alloc += size;
    assert_not_null(sa_ms_byte, size, index_alloc);
    for(i = 0; i < ref_seq_len; i++)
    {
        sa_ls_word[i] = sa_bwt[i] & 0xffffffff;
        sa_ms_byte[i] = (sa_bwt[i] >> 32) & 0xff;
    }
    outstream.write((char*)sa_ms_byte, ref_seq_len * sizeof(int8_t));
    outstream.write((char*)sa_ls_word, ref_seq_len * sizeof(uint32_t));
    outstream.close();
    printf("max_occ_ind = %ld\n", i >> CP_SHIFT_AVX);
    fflush(stdout);

    _mm_free(sa_ms_byte);
    _mm_free(sa_ls_word);
    return 0;
}

int FMI_search::build_fm_index_avx_mmap(const char *ref_file_name, char *binary_seq, int64_t ref_seq_len, int64_t *sa_bwt, int64_t *count) {
    printf("ref_seq_len = %ld\n", ref_seq_len);
    fflush(stdout);

    char outname[200];
    sprintf(outname, "%s.bwt.8bit.%d", ref_file_name, CP_BLOCK_SIZE_AVX);
    int64_t cp_occ_size = (ref_seq_len >> CP_SHIFT_AVX) + 1;

    ref_seq_len++;
    // Calculate the total size of the file
    size_t total_size = 0;
    total_size += 6*sizeof(int64_t); // Headers: ref_seq_len, count
    printf("Size: %d\n", total_size);
    total_size += cp_occ_size * sizeof(CP_OCC_AVX); // cp_occ
    printf("Size: %d\n", total_size);
    total_size += ref_seq_len * sizeof(int8_t); // sa_ms_byte
    printf("Size: %d\n", total_size);
    total_size += ref_seq_len * sizeof(uint32_t); // sa_ls_word
    // total_size += 5;

    // std::fstream outstream (outname, std::ios::out | std::ios::binary);
    // outstream.seekg(0);
    // Create the output file
    int fd = open(outname, O_CREAT|O_RDWR, 0644);
    ftruncate(fd, total_size);
    printf("Created file %s with size %d\n", outname, total_size);

    char *map = (char *)mmap(NULL, total_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    char *map_init = map;
    // printf("Map position= %p\n",  (void*)map);
    close(fd);

    printf("count = %ld, %ld, %ld, %ld, %ld\n", count[0], count[1], count[2], count[3], count[4]);
    fflush(stdout);


    memcpy(map, (char*)(&ref_seq_len), 1*sizeof(int64_t));
    map += 1 * sizeof(int64_t);
    memcpy(map, (char*)count, 5*sizeof(int64_t));
    map += 5 * sizeof(int64_t);
    // printf("Map position after 6 int64_t= %p\n",  (void*)map);

    // outstream.write((char *)(&ref_seq_len), 1 * sizeof(int64_t));
    // outstream.write((char*)count, 5 * sizeof(int64_t);
    // printf("6 int64_t me ocupan %d\n", 6*sizeof(int64_t));
    // int eof = outstream.tellp();
    // outstream.seekp(0, std::ios::end);
    // printf("Checa fmindex los primeros bergatrones ocupan %d\n", eof );
    // outstream.seekp(eof);

    int64_t i;
    int64_t ref_seq_len_aligned = ((ref_seq_len + CP_BLOCK_SIZE_AVX - 1) / CP_BLOCK_SIZE_AVX) * CP_BLOCK_SIZE_AVX;
    int64_t size = ref_seq_len_aligned * sizeof(uint8_t);
    // MMAP SCIENCE
    int fd_bwt = open("aux_bwt_avx", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_bwt, size);
    printf("Created file aux_bwt_avx with size %d\n", size);
    uint8_t *bwt = (uint8_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_bwt, 0);
    close(fd_bwt);
    int64_t bwt_size = size;

    // bwt = (uint8_t *)_mm_malloc(size, 64);

    index_alloc += size;
    // assert_not_null(bwt, size, index_alloc);

    for(i=0; i< ref_seq_len; i++)
    {
        if(sa_bwt[i] == 0)
        {
            bwt[i] = 4;
            printf("BWT[%ld] = 4\n", i);
        }
        else
        {
            char c = binary_seq[sa_bwt[i]-1];
            switch(c)
            {
                case 0: bwt[i] = 0;
                          break;
                case 1: bwt[i] = 1;
                          break;
                case 2: bwt[i] = 2;
                          break;
                case 3: bwt[i] = 3;
                          break;
                default:
                        fprintf(stderr, "ERROR! i = %ld, c = %c\n", i, c);
                        exit(EXIT_FAILURE);
            }
        }
    }
    for(i = ref_seq_len; i < ref_seq_len_aligned; i++)
        bwt[i] = DUMMY_CHAR;


    printf("CP_SHIFT = %d, CP_MASK = %d\n", CP_SHIFT_AVX, CP_MASK_AVX);
    printf("sizeof CP_OCC = %ld\n", sizeof(CP_OCC_AVX));
    fflush(stdout);
    // create checkpointed occ
    // int64_t cp_occ_size = (ref_seq_len >> CP_SHIFT_AVX) + 1;
    // CP_OCC_AVX *cp_occ = NULL;

    size = cp_occ_size * sizeof(CP_OCC_AVX);
    // cp_occ = (CP_OCC_AVX *)_mm_malloc(size, 64);
    index_alloc += size;
    // assert_not_null(cp_occ, size, index_alloc);
    // memset(cp_occ, 0, cp_occ_size * sizeof(CP_OCC_AVX));
    int64_t cp_count[16];

    memset(cp_count, 0, 16 * sizeof(int64_t));
    for(i = 0; i < ref_seq_len; i++)
    {
        if((i & CP_MASK_AVX) == 0)
        {
            CP_OCC_AVX cpo;
            cpo.cp_count[0] = cp_count[0];
            cpo.cp_count[1] = cp_count[1];
            cpo.cp_count[2] = cp_count[2];
            cpo.cp_count[3] = cp_count[3];
			memcpy(cpo.bwt_str, bwt + i, CP_BLOCK_SIZE_AVX * sizeof(uint8_t));

            // cp_occ[i >> CP_SHIFT_AVX] = cpo;
            // int disp = i >> CP_SHIFT_AVX;
            // if(disp < )
            memcpy(&map[i >> CP_SHIFT_AVX], (char*)(&cpo), sizeof(CP_OCC_AVX));
        }
        cp_count[bwt[i]]++;
    }
    // outstream.write((char*)cp_occ, cp_occ_size * sizeof(CP_OCC_AVX));
    // memcpy(map, (char*)cp_occ, cp_occ_size * sizeof(CP_OCC_AVX));
    map += cp_occ_size * sizeof(CP_OCC_AVX);
    // printf("Map position after cp_occ= %p\n",  (void*)map);

    // eof = outstream.tellp();
    // outstream.seekp(0, std::ios::end);
    // printf("Checa fmindex despues de escribir el cp_occ ocupa %d\n", eof);
    // outstream.seekp(eof);
    // _mm_free(cp_occ);
    // _mm_free(bwt);
    munmap(bwt, bwt_size);
    remove ("aux_bwt_avx");
    index_alloc -= (ref_seq_len_aligned * sizeof(uint8_t) + cp_occ_size * sizeof(CP_OCC_AVX));

    size = ref_seq_len * sizeof(uint32_t);
    // printf("CHECA EL TAMANIO DE LOS SA_LS BERGAS ES %d\n", size);
    // uint32_t *sa_ls_word = (uint32_t *)_mm_malloc(size, 64);
    // MMAP SCIENCE
    int fd_sa_ls = open("aux_sa_ls_word", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_sa_ls, size);
    printf("Created file aux_sa_ls_word with size %d\n", size);
    uint32_t *sa_ls_word = (uint32_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_sa_ls, 0);
    close(fd_sa_ls);

    // index_alloc += size;
    // assert_not_null(sa_ls_word, size, index_alloc);
    size = ref_seq_len * sizeof(int8_t);
    // int8_t *sa_ms_byte = (int8_t *)_mm_malloc(size, 64);
    int fd_sa_ms = open("aux_sa_ms_byte", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_sa_ms, size);
    printf("Created file aux_sa_ms_byte with size %d\n", size);
    int8_t *sa_ms_byte = (int8_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_sa_ms, 0);
    close(fd_sa_ms);
    // index_alloc += size;
    // assert_not_null(sa_ms_byte, size, index_alloc);

    for(i = 0; i < ref_seq_len; i++)
      map[i] = sa_bwt[i] & 0xffffffff;

    map += ref_seq_len*sizeof(int8_t);

    for(i = 0; i < ref_seq_len; i++)
      map[i] = (sa_bwt[i] >> 32) & 0xff;

    map += ref_seq_len*sizeof(uint32_t);
    // for(i = 0; i < ref_seq_len; i++)
    // {
    //     sa_ls_word[i] = sa_bwt[i] & 0xffffffff;
    //     sa_ms_byte[i] = (sa_bwt[i] >> 32) & 0xff;
    // }
    // // outstream.write((char*)sa_ms_byte, ref_seq_len * sizeof(int8_t));
    // memcpy(map, (char*)sa_ms_byte, ref_seq_len*sizeof(int8_t));
    // map += ref_seq_len*sizeof(int8_t);
    // printf("Map position after sa_ms_byte = %p\n",  (void*)map);
    //
    // memcpy(map, (char*)sa_ls_word, ref_seq_len * sizeof(uint32_t));
    // map += ref_seq_len*sizeof(uint32_t);
    // printf("Map position after sa_ls_word = %p\n",  (void*)map);

    // outstream.write((char*)sa_ls_word, ref_seq_len * sizeof(uint32_t));

    // eof = outstream.tellp();
    // outstream.seekp(0, std::ios::end);
    // printf("Checa fmindex despues de escribir los ultimos bergas %d\n", eof);
    // outstream.seekp(eof);
    // outstream.close();
    munmap(map_init, total_size);
    printf("max_occ_ind = %ld\n", i >> CP_SHIFT_AVX);
    fflush(stdout);

    // _mm_free(sa_ms_byte);
    // _mm_free(sa_ls_word);
    munmap(sa_ls_word, ref_seq_len * sizeof(uint32_t));
    remove("aux_sa_ls_word");
    munmap(sa_ms_byte, ref_seq_len * sizeof(int8_t));
    remove("aux_sa_ms_byte");
    // printf("Adios a la funsion fm index avx mmap bye\n");
    return 0;
}

int FMI_search::build_fm_index_scalar_mmap(const char *ref_file_name, char *binary_seq, int64_t ref_seq_len, int64_t *sa_bwt, int64_t *count) {
    printf("ref_seq_len = %ld\n", ref_seq_len);
    fflush(stdout);

    char outname[200];
    sprintf(outname, "%s.bwt.2bit.%d", ref_file_name, CP_BLOCK_SIZE_SCALAR);


    // std::fstream outstream (outname, std::ios::out | std::ios::binary);
    // outstream.seekg(0);

    printf("count = %ld, %ld, %ld, %ld, %ld\n", count[0], count[1], count[2], count[3], count[4]);
    fflush(stdout);

    ref_seq_len++;
    int64_t cp_occ_size = (ref_seq_len >> CP_SHIFT_SCALAR) + 1;

    // Calculate the total size of the file
    size_t total_size = 0;
    total_size += 6*sizeof(int64_t); // Headers: ref_seq_len, count
    // printf("Size: %d\n", total_size);
    total_size += cp_occ_size * sizeof(CP_OCC_AVX); // cp_occ
    // printf("Size: %d\n", total_size);
    total_size += ref_seq_len * sizeof(int8_t); // sa_ms_byte
    // printf("Size: %d\n", total_size);
    total_size += ref_seq_len * sizeof(uint32_t); // sa_ls_word
    // total_size += 5;


    // Create the output file
    int fd = open(outname, O_CREAT|O_RDWR, 0644);
    ftruncate(fd, total_size);
    printf("Created file %s with size %d\n", outname, total_size);

    char *map = (char *)mmap(NULL, total_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    char *map_init = map;
    // printf("Map position= %p\n",  (void*)map);
    close(fd);

    // outstream.write((char *)(&ref_seq_len), 1 * sizeof(int64_t));
    // outstream.write((char*)count, 5 * sizeof(int64_t));
    memcpy(map, (char*)(&ref_seq_len), 1*sizeof(int64_t));
    map += 1 * sizeof(int64_t);
    memcpy(map, (char*)count, 5*sizeof(int64_t));
    map += 5 * sizeof(int64_t);
    // printf("Map position after 6 int64_t= %p\n",  (void*)map);

    int64_t i;
    int64_t ref_seq_len_aligned = ((ref_seq_len + CP_BLOCK_SIZE_SCALAR - 1) / CP_BLOCK_SIZE_SCALAR) * CP_BLOCK_SIZE_SCALAR;
    int64_t size = ref_seq_len_aligned * sizeof(uint8_t);
    // MMAP SCIENCE
    int fd_bwt = open("aux_bwt_scalar", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_bwt, size);
    int64_t bwt_size = size;
    printf("Created file aux_bwt_scalar with size %d\n", size);
    uint8_t *bwt = (uint8_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_bwt, 0);
    close(fd_bwt);

    // bwt = (uint8_t *)_mm_malloc(size, 64);
    // assert_not_null(bwt, size, index_alloc);

    for(i=0; i< ref_seq_len; i++)
    {
        if(sa_bwt[i] == 0)
        {
            bwt[i] = 4;
            printf("BWT[%ld] = 4\n", i);
        }
        else
        {
            char c = binary_seq[sa_bwt[i]-1];
            switch(c)
            {
                case 0: bwt[i] = 0;
                          break;
                case 1: bwt[i] = 1;
                          break;
                case 2: bwt[i] = 2;
                          break;
                case 3: bwt[i] = 3;
                          break;
                default:
                        fprintf(stderr, "ERROR! i = %ld, c = %c\n", i, c);
                        exit(EXIT_FAILURE);
            }
        }
    }
    for(i = ref_seq_len; i < ref_seq_len_aligned; i++)
        bwt[i] = DUMMY_CHAR;


    printf("CP_SHIFT = %d, CP_MASK = %d\n", CP_SHIFT_SCALAR, CP_MASK_SCALAR);
    printf("sizeof CP_OCC = %ld\n", sizeof(CP_OCC_SCALAR));
    fflush(stdout);
    // create checkpointed occ
    // int64_t cp_occ_size = (ref_seq_len >> CP_SHIFT_SCALAR) + 1;
    // CP_OCC_SCALAR *cp_occ = NULL;

    size = cp_occ_size * sizeof(CP_OCC_SCALAR);
    // cp_occ = (CP_OCC_SCALAR *)_mm_malloc(size, 64);
    // assert_not_null(cp_occ, size, index_alloc);
    // memset(cp_occ, 0, cp_occ_size * sizeof(CP_OCC_SCALAR));
    int64_t cp_count[16];

    memset(cp_count, 0, 16 * sizeof(int64_t));
    for(i = 0; i < ref_seq_len; i++)
    {
        if((i & CP_MASK_SCALAR) == 0)
        {
            CP_OCC_SCALAR cpo;
            cpo.cp_count[0] = cp_count[0];
            cpo.cp_count[1] = cp_count[1];
            cpo.cp_count[2] = cp_count[2];
            cpo.cp_count[3] = cp_count[3];

			BIT_DATA_TYPE bwt_str_bit0 = 0;
			BIT_DATA_TYPE bwt_str_bit1 = 0;
			BIT_DATA_TYPE dollar_mask = 0;
			int32_t j;
			for(j = 0; j < CP_BLOCK_SIZE_SCALAR; j++)
			{
				uint8_t c = bwt[i + j];
				if((c == 4) || (c == DUMMY_CHAR))
				{
					dollar_mask <<= 1;
					dollar_mask += 1;
					c = 0;
				}
				else if(c > 3)
				{
					fprintf(stderr, "ERROR! [%ld, %d] c = %u\n", (long)i, j, c);
					exit(EXIT_FAILURE);
				}
				else
				{
					dollar_mask <<= 1;
					dollar_mask += 0;
				}
				bwt_str_bit0 = bwt_str_bit0 << 1;
				bwt_str_bit0 += (c & 1);
				bwt_str_bit1 = bwt_str_bit1 << 1;
				bwt_str_bit1 += ((c >> 1) & 1);
			}
			cpo.bwt_str_bit0 = bwt_str_bit0;
			cpo.bwt_str_bit1 = bwt_str_bit1;
			cpo.dollar_mask  = dollar_mask;

            memset(cpo.pad, 0, PADDING_SCALAR);
            // cp_occ[i >> CP_SHIFT_SCALAR] = cpo;
            memcpy(&map[i >> CP_SHIFT_SCALAR], (char*)(&cpo), sizeof(CP_OCC_SCALAR));
        }
        cp_count[bwt[i]]++;
    }
    map += cp_occ_size * sizeof(CP_OCC_SCALAR);
    munmap(bwt, bwt_size);
    remove("aux_bwt_scalar");
    // outstream.write((char*)cp_occ, cp_occ_size * sizeof(CP_OCC_SCALAR));
    // _mm_free(cp_occ);
    // _mm_free(bwt);

    size = ref_seq_len * sizeof(uint32_t);
    // uint32_t *sa_ls_word = (uint32_t *)_mm_malloc(size, 64);
    // assert_not_null(sa_ls_word, size, index_alloc);
    // MMAP SCIENCE
    int fd_sa_ls = open("aux_sa_ls_word", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_sa_ls, size);
    printf("Created file aux_sa_ls_word with size %d\n", size);
    uint32_t *sa_ls_word = (uint32_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_sa_ls, 0);
    close(fd_sa_ls);

    size = ref_seq_len * sizeof(int8_t);
    // int8_t *sa_ms_byte = (int8_t *)_mm_malloc(size, 64);
    // assert_not_null(sa_ms_byte, size, index_alloc);
    int fd_sa_ms = open("aux_sa_ms_byte", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_sa_ms, size);
    printf("Created file aux_sa_ms_byte with size %d\n", size);
    int8_t *sa_ms_byte = (int8_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_sa_ms, 0);
    close(fd_sa_ms);

    for(i = 0; i < ref_seq_len; i++)
      map[i] = sa_bwt[i] & 0xffffffff;

    map += ref_seq_len*sizeof(int8_t);

    for(i = 0; i < ref_seq_len; i++)
      map[i] = (sa_bwt[i] >> 32) & 0xff;

    map += ref_seq_len*sizeof(uint32_t);

    // for(i = 0; i < ref_seq_len; i++)
    // {
    //     sa_ls_word[i] = sa_bwt[i] & 0xffffffff;
    //     sa_ms_byte[i] = (sa_bwt[i] >> 32) & 0xff;
    // }

    // outstream.write((char*)sa_ms_byte, ref_seq_len * sizeof(int8_t));
    // outstream.write((char*)sa_ls_word, ref_seq_len * sizeof(uint32_t));
    // outstream.close();
    munmap(map_init, total_size);
    printf("max_occ_ind = %ld\n", i >> CP_SHIFT_SCALAR);
    fflush(stdout);

    // _mm_free(sa_ms_byte);
    // _mm_free(sa_ls_word);
    munmap(sa_ls_word, ref_seq_len * sizeof(uint32_t));
    remove("aux_sa_ls_word");
    munmap(sa_ms_byte, ref_seq_len * sizeof(int8_t));
    remove("aux_sa_ms_byte");
    return 0;
}


int FMI_search::build_fm_index_scalar(const char *ref_file_name, char *binary_seq, int64_t ref_seq_len, int64_t *sa_bwt, int64_t *count) {
    printf("ref_seq_len = %ld\n", ref_seq_len);
    fflush(stdout);

    char outname[200];

    sprintf(outname, "%s.bwt.2bit.%d", ref_file_name, CP_BLOCK_SIZE_SCALAR);

    std::fstream outstream (outname, std::ios::out | std::ios::binary);
    outstream.seekg(0);

    printf("count = %ld, %ld, %ld, %ld, %ld\n", count[0], count[1], count[2], count[3], count[4]);
    fflush(stdout);

    uint8_t *bwt;

    ref_seq_len++;
    outstream.write((char *)(&ref_seq_len), 1 * sizeof(int64_t));
    outstream.write((char*)count, 5 * sizeof(int64_t));

    int64_t i;
    int64_t ref_seq_len_aligned = ((ref_seq_len + CP_BLOCK_SIZE_SCALAR - 1) / CP_BLOCK_SIZE_SCALAR) * CP_BLOCK_SIZE_SCALAR;
    int64_t size = ref_seq_len_aligned * sizeof(uint8_t);
    bwt = (uint8_t *)_mm_malloc(size, 64);
    assert_not_null(bwt, size, index_alloc);

    for(i=0; i< ref_seq_len; i++)
    {
        if(sa_bwt[i] == 0)
        {
            bwt[i] = 4;
            printf("BWT[%ld] = 4\n", i);
        }
        else
        {
            char c = binary_seq[sa_bwt[i]-1];
            switch(c)
            {
                case 0: bwt[i] = 0;
                          break;
                case 1: bwt[i] = 1;
                          break;
                case 2: bwt[i] = 2;
                          break;
                case 3: bwt[i] = 3;
                          break;
                default:
                        fprintf(stderr, "ERROR! i = %ld, c = %c\n", i, c);
                        exit(EXIT_FAILURE);
            }
        }
    }
    for(i = ref_seq_len; i < ref_seq_len_aligned; i++)
        bwt[i] = DUMMY_CHAR;


    printf("CP_SHIFT = %d, CP_MASK = %d\n", CP_SHIFT_SCALAR, CP_MASK_SCALAR);
    printf("sizeof CP_OCC = %ld\n", sizeof(CP_OCC_SCALAR));
    fflush(stdout);
    // create checkpointed occ
    int64_t cp_occ_size = (ref_seq_len >> CP_SHIFT_SCALAR) + 1;
    CP_OCC_SCALAR *cp_occ = NULL;

    size = cp_occ_size * sizeof(CP_OCC_SCALAR);
    cp_occ = (CP_OCC_SCALAR *)_mm_malloc(size, 64);
    assert_not_null(cp_occ, size, index_alloc);
    memset(cp_occ, 0, cp_occ_size * sizeof(CP_OCC_SCALAR));
    int64_t cp_count[16];

    memset(cp_count, 0, 16 * sizeof(int64_t));
    for(i = 0; i < ref_seq_len; i++)
    {
        if((i & CP_MASK_SCALAR) == 0)
        {
            CP_OCC_SCALAR cpo;
            cpo.cp_count[0] = cp_count[0];
            cpo.cp_count[1] = cp_count[1];
            cpo.cp_count[2] = cp_count[2];
            cpo.cp_count[3] = cp_count[3];

			BIT_DATA_TYPE bwt_str_bit0 = 0;
			BIT_DATA_TYPE bwt_str_bit1 = 0;
			BIT_DATA_TYPE dollar_mask = 0;
			int32_t j;
			for(j = 0; j < CP_BLOCK_SIZE_SCALAR; j++)
			{
				uint8_t c = bwt[i + j];
				if((c == 4) || (c == DUMMY_CHAR))
				{
					dollar_mask <<= 1;
					dollar_mask += 1;
					c = 0;
				}
				else if(c > 3)
				{
					fprintf(stderr, "ERROR! [%ld, %d] c = %u\n", (long)i, j, c);
					exit(EXIT_FAILURE);
				}
				else
				{
					dollar_mask <<= 1;
					dollar_mask += 0;
				}
				bwt_str_bit0 = bwt_str_bit0 << 1;
				bwt_str_bit0 += (c & 1);
				bwt_str_bit1 = bwt_str_bit1 << 1;
				bwt_str_bit1 += ((c >> 1) & 1);
			}
			cpo.bwt_str_bit0 = bwt_str_bit0;
			cpo.bwt_str_bit1 = bwt_str_bit1;
			cpo.dollar_mask  = dollar_mask;

            memset(cpo.pad, 0, PADDING_SCALAR);
            cp_occ[i >> CP_SHIFT_SCALAR] = cpo;
        }
        cp_count[bwt[i]]++;
    }
    outstream.write((char*)cp_occ, cp_occ_size * sizeof(CP_OCC_SCALAR));
    _mm_free(cp_occ);
    _mm_free(bwt);

    size = ref_seq_len * sizeof(uint32_t);
    uint32_t *sa_ls_word = (uint32_t *)_mm_malloc(size, 64);
    assert_not_null(sa_ls_word, size, index_alloc);
    size = ref_seq_len * sizeof(int8_t);
    int8_t *sa_ms_byte = (int8_t *)_mm_malloc(size, 64);
    assert_not_null(sa_ms_byte, size, index_alloc);
    for(i = 0; i < ref_seq_len; i++)
    {
        sa_ls_word[i] = sa_bwt[i] & 0xffffffff;
        sa_ms_byte[i] = (sa_bwt[i] >> 32) & 0xff;
    }
    outstream.write((char*)sa_ms_byte, ref_seq_len * sizeof(int8_t));
    outstream.write((char*)sa_ls_word, ref_seq_len * sizeof(uint32_t));
    outstream.close();
    printf("max_occ_ind = %ld\n", i >> CP_SHIFT_SCALAR);
    fflush(stdout);

    _mm_free(sa_ms_byte);
    _mm_free(sa_ls_word);
    return 0;
}

int FMI_search::build_index() {

    char *prefix = file_name;
    uint64_t startTick;
    startTick = __rdtsc();
    index_alloc = 0;

    std::string reference_seq;
    char pac_file_name[200];
    sprintf(pac_file_name, "%s.pac", prefix);
    pac2nt(pac_file_name, reference_seq);
	int64_t pac_len = reference_seq.length();
    int status;
    int64_t size = pac_len * sizeof(char);
    char *binary_ref_seq = (char *)_mm_malloc(size, 64);
    index_alloc += size;
    assert_not_null(binary_ref_seq, size, index_alloc);
    char binary_ref_name[200];
    sprintf(binary_ref_name, "%s.0123", prefix);
    std::fstream binary_ref_stream (binary_ref_name, std::ios::out | std::ios::binary);
    binary_ref_stream.seekg(0);
    fprintf(stderr, "init ticks = %ld\n", __rdtsc() - startTick);
    startTick = __rdtsc();
    int64_t i, count[16];
	memset(count, 0, sizeof(int64_t) * 16);
    for(i = 0; i < pac_len; i++)
    {
        switch(reference_seq[i])
        {
            case 'A':
            binary_ref_seq[i] = 0, ++count[0];
            break;
            case 'C':
            binary_ref_seq[i] = 1, ++count[1];
            break;
            case 'G':
            binary_ref_seq[i] = 2, ++count[2];
            break;
            case 'T':
            binary_ref_seq[i] = 3, ++count[3];
            break;
            default:
            binary_ref_seq[i] = 4;

        }
    }
    count[4]=count[0]+count[1]+count[2]+count[3];
    count[3]=count[0]+count[1]+count[2];
    count[2]=count[0]+count[1];
    count[1]=count[0];
    count[0]=0;
    fprintf(stderr, "ref seq len = %ld\n", pac_len);
    binary_ref_stream.write(binary_ref_seq, pac_len * sizeof(char));
    fprintf(stderr, "binary seq ticks = %ld\n", __rdtsc() - startTick);
    startTick = __rdtsc();

    size = (pac_len + 2) * sizeof(int64_t);
    int64_t *suffix_array=(int64_t *)_mm_malloc(size, 64);
    index_alloc += size;
    assert_not_null(suffix_array, size, index_alloc);
    startTick = __rdtsc();
	//status = saisxx<const char *, int64_t *, int64_t>(reference_seq.c_str(), suffix_array + 1, pac_len, 4);
	status = saisxx(reference_seq.c_str(), suffix_array + 1, pac_len);
	suffix_array[0] = pac_len;
    fprintf(stderr, "build index ticks = %ld\n", __rdtsc() - startTick);
    startTick = __rdtsc();

    build_fm_index_avx(prefix, binary_ref_seq, pac_len, suffix_array, count);
	build_fm_index_scalar(prefix, binary_ref_seq, pac_len, suffix_array, count);

    _mm_free(binary_ref_seq);
    _mm_free(suffix_array);
    return 0;
}

int FMI_search::build_index_mmap() {

    char *prefix = file_name;
    uint64_t startTick;
    startTick = __rdtsc();
    index_alloc = 0;

    // std::string reference_seq;
    char *reference_seq;
    char pac_file_name[200];

    sprintf(pac_file_name, "%s.pac", prefix);
    // pac2nt_mmap(pac_file_name, reference_seq);
	  // int64_t pac_len = reference_seq.length();

	  int64_t pac_len = pac2nt_mmap(pac_file_name, &reference_seq);
    // printf("Desde el llamante me da que reference_seq es %p\n",(void*)(reference_seq));
    int status;
    int64_t size = pac_len * sizeof(char);
    // char *binary_ref_seq = (char *)_mm_malloc(size, 64);
    // index_alloc += size;
    // assert_not_null(binary_ref_seq, size, index_alloc);
    char binary_ref_name[200];
    sprintf(binary_ref_name, "%s.0123", prefix);

    // Create the file and the mapping
    int fd = open(binary_ref_name, O_CREAT|O_RDWR, 0644);
    ftruncate(fd, pac_len * sizeof(char));
    printf("Created file %s with size %d\n", binary_ref_name, pac_len * sizeof(char));
    char *map = (char *)mmap(NULL, pac_len * sizeof(char), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);

    // std::fstream binary_ref_stream (binary_ref_name, std::ios::out | std::ios::binary);
    // binary_ref_stream.seekg(0);
    fprintf(stderr, "init ticks = %ld\n", __rdtsc() - startTick);
    startTick = __rdtsc();
    int64_t i, count[16];
	memset(count, 0, sizeof(int64_t) * 16);
    for(i = 0; i < pac_len; i++)
    {
      // printf("%d\n", i);
        switch(reference_seq[i])
        {
            case 'A':
            // binary_ref_seq[i] = 0, ++count[0];
            map[i] =  0, ++count[0];
            break;
            case 'C':
            // binary_ref_seq[i] = 1, ++count[1];
            map[i] =  1, ++count[1];
            break;
            case 'G':
            // binary_ref_seq[i] = 2, ++count[2];
            map[i] =  2, ++count[2];
            break;
            case 'T':
            // binary_ref_seq[i] = 3, ++count[3];
            map[i] =  3, ++count[3];
            break;
            default:
            map[i] =  4;
            // binary_ref_seq[i] = 4;

        }
    }
    count[4]=count[0]+count[1]+count[2]+count[3];
    count[3]=count[0]+count[1]+count[2];
    count[2]=count[0]+count[1];
    count[1]=count[0];
    count[0]=0;
    fprintf(stderr, "ref seq len = %ld\n", pac_len);
    // binary_ref_stream.write(binary_ref_seq, pac_len * sizeof(char));
    fprintf(stderr, "binary seq ticks = %ld\n", __rdtsc() - startTick);
    startTick = __rdtsc();

    size = (pac_len + 2) * sizeof(int64_t);
    // int64_t *suffix_array=(int64_t *)_mm_malloc(size, 64);
    // MMAP SCIENCE
    int fd_sa = open("aux_suffix_array", O_CREAT|O_RDWR, 0644);
    ftruncate(fd_sa, size);
    printf("Created file aux_suffix_array with size %d\n", size);
    int64_t *suffix_array = (int64_t *)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_sa, 0);
    close(fd_sa);

    // index_alloc += size;
    // assert_not_null(suffix_array, size, index_alloc);
    startTick = __rdtsc();
	//status = saisxx<const char *, int64_t *, int64_t>(reference_seq.c_str(), suffix_array + 1, pac_len, 4);
	// status = saisxx(reference_seq.c_str(), suffix_array + 1, pac_len);
  printf("Antes de saisxx\n");
  status = saisxx(reference_seq, suffix_array + 1, pac_len);
  printf("Despues de saisxx\n");


  // TODO: ver si se puede procesar por cachos
	suffix_array[0] = pac_len;
    fprintf(stderr, "build index ticks = %ld\n", __rdtsc() - startTick);
    startTick = __rdtsc();
    remove("aux_ref_seq");
    // TODO: acabar esto
    munmap(reference_seq, pac_len * sizeof(char));
    remove("aux_ref_seq");

    build_fm_index_avx_mmap(prefix, map, pac_len, suffix_array, count);
	build_fm_index_scalar_mmap(prefix, map, pac_len, suffix_array, count);
  munmap(map, pac_len * sizeof(char) );
    // _mm_free(binary_ref_seq);
    // _mm_free(suffix_array);
    munmap(suffix_array, size);
    remove ("aux_suffix_array");
    return 0;
}

void FMI_search::load_index()
{
    char *ref_file_name = file_name;
    //beCalls = 0;
    char cp_file_name[1000];
    assert(strnlen(ref_file_name, 1000) + 12 < 1000);
#if ((!__AVX2__))
    sprintf(cp_file_name, "%s.bwt.2bit.%d", ref_file_name, CP_BLOCK_SIZE_SCALAR);
#else
    sprintf(cp_file_name, "%s.bwt.8bit.%d", ref_file_name, CP_BLOCK_SIZE_AVX);
#endif
    // Read the BWT and FM index of the reference sequence
    FILE *cpstream = NULL;
    cpstream = fopen(cp_file_name,"rb");
    if (cpstream == NULL)
    {
        fprintf(stderr, "ERROR! Unable to open the file: %s\n", cp_file_name);
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf(stderr, "* Index file found. Loading index from %s\n", cp_file_name);
    }

    err_fread_noeof(&reference_seq_len, sizeof(int64_t), 1, cpstream);
    assert(reference_seq_len > 0);
    assert(reference_seq_len <= 0x7fffffffffL);

    fprintf(stderr, "* Reference seq len for bi-index = %ld\n", reference_seq_len);

    // create checkpointed occ
    int64_t cp_occ_size = (reference_seq_len >> CP_SHIFT) + 1;
    cp_occ = NULL;

    err_fread_noeof(&count[0], sizeof(int64_t), 5, cpstream);
    if ((cp_occ = (CP_OCC *)_mm_malloc(cp_occ_size * sizeof(CP_OCC), 64)) == NULL) {
        fprintf(stderr, "ERROR! unable to allocated cp_occ memory\n");
        exit(EXIT_FAILURE);
    }

    err_fread_noeof(cp_occ, sizeof(CP_OCC), cp_occ_size, cpstream);
    int64_t ii = 0;
    for(ii = 0; ii < 5; ii++)// update read count structure
    {
        count[ii] = count[ii] + 1;
    }
    sa_ms_byte = (int8_t *)_mm_malloc(reference_seq_len * sizeof(int8_t), 64);
    sa_ls_word = (uint32_t *)_mm_malloc(reference_seq_len * sizeof(uint32_t), 64);
    err_fread_noeof(sa_ms_byte, sizeof(int8_t), reference_seq_len, cpstream);
    err_fread_noeof(sa_ls_word, sizeof(uint32_t), reference_seq_len, cpstream);
    fclose(cpstream);

    sentinel_index = -1;
    int64_t x;
    for(x = 0; x < reference_seq_len; x++)
    {
        if(get_sa_entry(x) == 0)
            sentinel_index = x;
    }

    fprintf(stderr, "* Count:\n");
    for(x = 0; x < 5; x++)
    {
        fprintf(stderr, "%ld,\t%lu\n", x, (unsigned long)count[x]);
    }
    fprintf(stderr, "\n");

    fprintf(stderr, "* Reading other elements of the index from files %s\n",
            ref_file_name);
    bwa_idx_load_ele(ref_file_name, BWA_IDX_ALL);

#if ((!__AVX2__))
    base_mask[0][0] = 0;
    base_mask[0][1] = 0;
    base_mask[1][0] = 0xffffffffffffffffL;
    base_mask[1][1] = 0;
    base_mask[2][0] = 0;
    base_mask[2][1] = 0xffffffffffffffffL;
    base_mask[3][0] = 0xffffffffffffffffL;
    base_mask[3][1] = 0xffffffffffffffffL;
#else
    c_bcast_array = (uint8_t *)_mm_malloc(256 * sizeof(uint8_t), 64);
    for(ii = 0; ii < 4; ii++)
    {
        int32_t j;
        for(j = 0; j < 64; j++)
        {
            c_bcast_array[ii * 64 + j] = ii;
        }
    }
#endif
    fprintf(stderr, "* Done reading Index!!\n");
}

void FMI_search::getSMEMsOnePosOneThread(uint8_t *enc_qdb,
                                         int16_t *query_pos_array,
                                         int32_t *min_intv_array,
                                         int32_t *rid_array,
                                         int32_t numReads,
                                         int32_t batch_size,
                                         const bseq1_t *seq_,
                                         int32_t *query_cum_len_ar,
                                         int32_t max_readlength,
                                         int32_t minSeedLen,
                                         SMEM *matchArray,
                                         int64_t *__numTotalSmem)
{
    int64_t numTotalSmem = *__numTotalSmem;
    SMEM prevArray[max_readlength];

    uint32_t i;
    // Perform SMEM for original reads
    for(i = 0; i < numReads; i++)
    {
        int x = query_pos_array[i];
        int32_t rid = rid_array[i];
        int next_x = x + 1;

        int readlength = seq_[rid].l_seq;
        int offset = query_cum_len_ar[rid];
        // uint8_t a = enc_qdb[rid * readlength + x];
        uint8_t a = enc_qdb[offset + x];

        if(a < 4)
        {
            SMEM smem;
            smem.rid = rid;
            smem.m = x;
            smem.n = x;
            smem.k = count[a];
            smem.l = count[3 - a];
            smem.s = count[a+1] - count[a];
            int numPrev = 0;

            int j;
            for(j = x + 1; j < readlength; j++)
            {
                // a = enc_qdb[rid * readlength + j];
                a = enc_qdb[offset + j];
                next_x = j + 1;
                if(a < 4)
                {
                    SMEM smem_ = smem;

                    // Forward extension is backward extension with the BWT of reverse complement
                    smem_.k = smem.l;
                    smem_.l = smem.k;
                    SMEM newSmem_ = backwardExt(smem_, 3 - a);
                    //SMEM newSmem_ = forwardExt(smem_, 3 - a);
                    SMEM newSmem = newSmem_;
                    newSmem.k = newSmem_.l;
                    newSmem.l = newSmem_.k;
                    newSmem.n = j;

                    int32_t s_neq_mask = newSmem.s != smem.s;

                    prevArray[numPrev] = smem;
                    numPrev += s_neq_mask;
                    if(newSmem.s < min_intv_array[i])
                    {
                        next_x = j;
                        break;
                    }
                    smem = newSmem;
#ifdef ENABLE_PREFETCH
                    _mm_prefetch((const char *)(&cp_occ[(smem.k) >> CP_SHIFT]), _MM_HINT_T0);
                    _mm_prefetch((const char *)(&cp_occ[(smem.l) >> CP_SHIFT]), _MM_HINT_T0);
#endif
                }
                else
                {
                    break;
                }
            }
            if(smem.s >= min_intv_array[i])
            {

                prevArray[numPrev] = smem;
                numPrev++;
            }

            SMEM *prev;
            prev = prevArray;

            int p;
            for(p = 0; p < (numPrev/2); p++)
            {
                SMEM temp = prev[p];
                prev[p] = prev[numPrev - p - 1];
                prev[numPrev - p - 1] = temp;
            }

            // Backward search
            int cur_j = readlength;
            for(j = x - 1; j >= 0; j--)
            {
                int numCurr = 0;
                int curr_s = -1;
                // a = enc_qdb[rid * readlength + j];
                a = enc_qdb[offset + j];

                if(a > 3)
                {
                    break;
                }
                for(p = 0; p < numPrev; p++)
                {
                    SMEM smem = prev[p];
                    SMEM newSmem = backwardExt(smem, a);
                    newSmem.m = j;

                    if((newSmem.s < min_intv_array[i]) && ((smem.n - smem.m + 1) >= minSeedLen))
                    {
                        cur_j = j;

                        matchArray[numTotalSmem++] = smem;
                        break;
                    }
                    if((newSmem.s >= min_intv_array[i]) && (newSmem.s != curr_s))
                    {
                        curr_s = newSmem.s;
                        prev[numCurr++] = newSmem;
#ifdef ENABLE_PREFETCH
                        _mm_prefetch((const char *)(&cp_occ[(newSmem.k) >> CP_SHIFT]), _MM_HINT_T0);
                        _mm_prefetch((const char *)(&cp_occ[(newSmem.k + newSmem.s) >> CP_SHIFT]), _MM_HINT_T0);
#endif
                        break;
                    }
                }
                p++;
                for(; p < numPrev; p++)
                {
                    SMEM smem = prev[p];

                    SMEM newSmem = backwardExt(smem, a);
                    newSmem.m = j;


                    if((newSmem.s >= min_intv_array[i]) && (newSmem.s != curr_s))
                    {
                        curr_s = newSmem.s;
                        prev[numCurr++] = newSmem;
#ifdef ENABLE_PREFETCH
                        _mm_prefetch((const char *)(&cp_occ[(newSmem.k) >> CP_SHIFT]), _MM_HINT_T0);
                        _mm_prefetch((const char *)(&cp_occ[(newSmem.k + newSmem.s) >> CP_SHIFT]), _MM_HINT_T0);
#endif
                    }
                }
                numPrev = numCurr;
                if(numCurr == 0)
                {
                    break;
                }
            }
            if(numPrev != 0)
            {
                SMEM smem = prev[0];
                if(((smem.n - smem.m + 1) >= minSeedLen))
                {

                    matchArray[numTotalSmem++] = smem;
                }
                numPrev = 0;
            }
        }
        query_pos_array[i] = next_x;
    }
    (*__numTotalSmem) = numTotalSmem;
}

void FMI_search::getSMEMsAllPosOneThread(uint8_t *enc_qdb,
                                         int32_t *min_intv_array,
                                         int32_t *rid_array,
                                         int32_t numReads,
                                         int32_t batch_size,
                                         const bseq1_t *seq_,
                                         int32_t *query_cum_len_ar,
                                         int32_t max_readlength,
                                         int32_t minSeedLen,
                                         SMEM *matchArray,
                                         int64_t *__numTotalSmem)
{
    int16_t *query_pos_array = (int16_t *)_mm_malloc(numReads * sizeof(int16_t), 64);

    int32_t i;
    for(i = 0; i < numReads; i++)
        query_pos_array[i] = 0;

    int32_t numActive = numReads;
    (*__numTotalSmem) = 0;

    do
    {
        int32_t head = 0;
        int32_t tail = 0;
        for(head = 0; head < numActive; head++)
        {
            int readlength = seq_[rid_array[head]].l_seq;
            if(query_pos_array[head] < readlength)
            {
                rid_array[tail] = rid_array[head];
                query_pos_array[tail] = query_pos_array[head];
                min_intv_array[tail] = min_intv_array[head];
                tail++;
            }
        }
        getSMEMsOnePosOneThread(enc_qdb,
                                query_pos_array,
                                min_intv_array,
                                rid_array,
                                tail,
                                batch_size,
                                seq_,
                                query_cum_len_ar,
                                max_readlength,
                                minSeedLen,
                                matchArray,
                                __numTotalSmem);
        numActive = tail;
    } while(numActive > 0);

    _mm_free(query_pos_array);
}

int64_t FMI_search::bwtSeedStrategyAllPosOneThread(uint8_t *enc_qdb,
                                                   int32_t *max_intv_array,
                                                   int32_t numReads,
                                                   const bseq1_t *seq_,
                                                   int32_t *query_cum_len_ar,
                                                   int32_t minSeedLen,
                                                   SMEM *matchArray)
{
    int32_t i;

    int64_t numTotalSeed = 0;

    for(i = 0; i < numReads; i++)
    {
        int readlength = seq_[i].l_seq;
        int16_t x = 0;
        while(x < readlength)
        {
            int next_x = x + 1;

            // Forward search
            SMEM smem;
            smem.rid = i;
            smem.m = x;
            smem.n = x;

            int offset = query_cum_len_ar[i];
            uint8_t a = enc_qdb[offset + x];
            // uint8_t a = enc_qdb[i * readlength + x];

            if(a < 4)
            {
                smem.k = count[a];
                smem.l = count[3 - a];
                smem.s = count[a+1] - count[a];


                int j;
                for(j = x + 1; j < readlength; j++)
                {
                    next_x = j + 1;
                    // a = enc_qdb[i * readlength + j];
                    a = enc_qdb[offset + j];
                    if(a < 4)
                    {
                        SMEM smem_ = smem;

                        // Forward extension is backward extension with the BWT of reverse complement
                        smem_.k = smem.l;
                        smem_.l = smem.k;
                        SMEM newSmem_ = backwardExt(smem_, 3 - a);
                        //SMEM smem = backwardExt(smem, 3 - a);
                        //smem.n = j;
                        SMEM newSmem = newSmem_;
                        newSmem.k = newSmem_.l;
                        newSmem.l = newSmem_.k;
                        newSmem.n = j;
                        smem = newSmem;
#ifdef ENABLE_PREFETCH
                        _mm_prefetch((const char *)(&cp_occ[(smem.k) >> CP_SHIFT]), _MM_HINT_T0);
                        _mm_prefetch((const char *)(&cp_occ[(smem.l) >> CP_SHIFT]), _MM_HINT_T0);
#endif


                        if((smem.s < max_intv_array[i]) && ((smem.n - smem.m + 1) >= minSeedLen))
                        {

                            if(smem.s > 0)
                            {
                                matchArray[numTotalSeed++] = smem;
                            }
                            break;
                        }
                    }
                    else
                    {

                        break;
                    }
                }

            }
            x = next_x;
        }
    }
    return numTotalSeed;
}


void FMI_search::getSMEMs(uint8_t *enc_qdb,
        int32_t numReads,
        int32_t batch_size,
        int32_t readlength,
        int32_t minSeedLen,
        int32_t nthreads,
        SMEM *matchArray,
        int64_t *numTotalSmem)
{
    SMEM *prevArray = (SMEM *)_mm_malloc(nthreads * readlength * sizeof(SMEM), 64);
    SMEM *currArray = (SMEM *)_mm_malloc(nthreads * readlength * sizeof(SMEM), 64);


// #pragma omp parallel num_threads(nthreads)
    {
        int tid = 0; //omp_get_thread_num();   // removed omp
        numTotalSmem[tid] = 0;
        SMEM *myPrevArray = prevArray + tid * readlength;
        SMEM *myCurrArray = prevArray + tid * readlength;

        int32_t perThreadQuota = (numReads + (nthreads - 1)) / nthreads;
        int32_t first = tid * perThreadQuota;
        int32_t last  = (tid + 1) * perThreadQuota;
        if(last > numReads) last = numReads;
        SMEM *myMatchArray = matchArray + first * readlength;

        uint32_t i;
        // Perform SMEM for original reads
        for(i = first; i < last; i++)
        {
            int x = readlength - 1;
            int numPrev = 0;
            int numSmem = 0;

            while (x >= 0)
            {
                // Forward search
                SMEM smem;
                smem.rid = i;
                smem.m = x;
                smem.n = x;
                uint8_t a = enc_qdb[i * readlength + x];

                if(a > 3)
                {
                    x--;
                    continue;
                }
                smem.k = count[a];
                smem.l = count[3 - a];
                smem.s = count[a+1] - count[a];

                int j;
                for(j = x + 1; j < readlength; j++)
                {
                    a = enc_qdb[i * readlength + j];
                    if(a < 4)
                    {
                        SMEM smem_ = smem;

                        // Forward extension is backward extension with the BWT of reverse complement
                        smem_.k = smem.l;
                        smem_.l = smem.k;
                        SMEM newSmem_ = backwardExt(smem_, 3 - a);
                        SMEM newSmem = newSmem_;
                        newSmem.k = newSmem_.l;
                        newSmem.l = newSmem_.k;
                        newSmem.n = j;

                        if(newSmem.s != smem.s)
                        {
                            myPrevArray[numPrev] = smem;
                            numPrev++;
                        }
                        smem = newSmem;
                        if(newSmem.s == 0)
                        {
                            break;
                        }
                    }
                    else
                    {
                        myPrevArray[numPrev] = smem;
                        numPrev++;
                        break;
                    }
                }
                if(smem.s != 0)
                {
                    myPrevArray[numPrev++] = smem;
                }

                SMEM *curr, *prev;
                prev = myPrevArray;
                curr = myCurrArray;

                int p;
                for(p = 0; p < (numPrev/2); p++)
                {
                    SMEM temp = prev[p];
                    prev[p] = prev[numPrev - p - 1];
                    prev[numPrev - p - 1] = temp;
                }

                int next_x = x - 1;

                // Backward search
                int cur_j = readlength;
                for(j = x - 1; j >= 0; j--)
                {
                    int numCurr = 0;
                    int curr_s = -1;
                    a = enc_qdb[i * readlength + j];
                    //printf("a = %d\n", a);
                    if(a > 3)
                    {
                        next_x = j - 1;
                        break;
                    }
                    for(p = 0; p < numPrev; p++)
                    {
                        SMEM smem = prev[p];
                        SMEM newSmem = backwardExt(smem, a);
                        newSmem.m = j;

                        if(newSmem.s == 0)
                        {
                            if((numCurr == 0) && (j < cur_j))
                            {
                                cur_j = j;
                                if((smem.n - smem.m + 1) >= minSeedLen)
                                    myMatchArray[numTotalSmem[tid] + numSmem++] = smem;
                            }
                        }
                        if((newSmem.s != 0) && (newSmem.s != curr_s))
                        {
                            curr_s = newSmem.s;
                            curr[numCurr++] = newSmem;
                        }
                    }
                    SMEM *temp = prev;
                    prev = curr;
                    curr = temp;
                    numPrev = numCurr;
                    if(numCurr == 0)
                    {
                        next_x = j;
                        break;
                    }
                    else
                    {
                        next_x = j - 1;
                    }
                }
                if(numPrev != 0)
                {
                    SMEM smem = prev[0];
                    if((smem.n - smem.m + 1) >= minSeedLen)
                        myMatchArray[numTotalSmem[tid] + numSmem++] = smem;
                    numPrev = 0;
                }
                x = next_x;
            }
            numTotalSmem[tid] += numSmem;
        }
    }

    _mm_free(prevArray);
    _mm_free(currArray);
}


int compare_smem(const void *a, const void *b)
{
    SMEM *pa = (SMEM *)a;
    SMEM *pb = (SMEM *)b;

    if(pa->rid < pb->rid)
        return -1;
    if(pa->rid > pb->rid)
        return 1;

    if(pa->m < pb->m)
        return -1;
    if(pa->m > pb->m)
        return 1;
    if(pa->n > pb->n)
        return -1;
    if(pa->n < pb->n)
        return 1;
    return 0;
}

void FMI_search::sortSMEMs(SMEM *matchArray,
        int64_t numTotalSmem[],
        int32_t numReads,
        int32_t readlength,
        int nthreads)
{
    int tid;
    int32_t perThreadQuota = (numReads + (nthreads - 1)) / nthreads;
    for(tid = 0; tid < nthreads; tid++)
    {
        int32_t first = tid * perThreadQuota;
        SMEM *myMatchArray = matchArray + first * readlength;
        qsort(myMatchArray, numTotalSmem[tid], sizeof(SMEM), compare_smem);
    }
}


SMEM FMI_search::backwardExt(SMEM smem, uint8_t a)
{
    //beCalls++;
    uint8_t b;

    int64_t k[4], l[4], s[4];
    for(b = 0; b < 4; b++)
    {
        int64_t sp = (int64_t)(smem.k);
        int64_t ep = (int64_t)(smem.k) + (int64_t)(smem.s);
#if ((!__AVX2__))
        GET_OCC(sp, b, occ_id_sp, y_sp, occ_sp, bwt_str_bit0_sp, bwt_str_bit1_sp, bit0_cmp_sp, bit1_cmp_sp, mismatch_mask_sp);
        GET_OCC(ep, b, occ_id_ep, y_ep, occ_ep, bwt_str_bit0_ep, bwt_str_bit1_ep, bit0_cmp_ep, bit1_cmp_ep, mismatch_mask_ep);
#else
        __m256i b256;
        b256 = _mm256_load_si256((const __m256i *)(c_bcast_array + b * 64));
        GET_OCC(sp, b, b256, occ_id_sp, y_sp, occ_sp, bwt_str_sp, bwt_sp_vec, mask_sp_vec, mask_sp);
        GET_OCC(ep, b, b256, occ_id_ep, y_ep, occ_ep, bwt_str_ep, bwt_ep_vec, mask_ep_vec, mask_ep);
#endif
        k[b] = count[b] + occ_sp;
        s[b] = occ_ep - occ_sp;
    }

    int64_t sentinel_offset = 0;
    if((smem.k <= sentinel_index) && ((smem.k + smem.s) > sentinel_index)) sentinel_offset = 1;
    l[3] = smem.l + sentinel_offset;
    l[2] = l[3] + s[3];
    l[1] = l[2] + s[2];
    l[0] = l[1] + s[1];

    smem.k = k[a];
    smem.l = l[a];
    smem.s = s[a];
    return smem;
}

int64_t FMI_search::get_sa_entry(int64_t pos)
{
    int64_t sa_entry = sa_ms_byte[pos];
    sa_entry = sa_entry << 32;
    sa_entry = sa_entry + sa_ls_word[pos];
    return sa_entry;
}

void FMI_search::get_sa_entries(int64_t *posArray, int64_t *coordArray, uint32_t count, int32_t nthreads)
{
    uint32_t i;
// #pragma omp parallel for num_threads(nthreads)
    for(i = 0; i < count; i++)
    {
        int64_t pos = posArray[i];
        int64_t sa_entry = sa_ms_byte[pos];
        sa_entry = sa_entry << 32;
        sa_entry = sa_entry + sa_ls_word[pos];
        //_mm_prefetch((const char *)(sa_ms_byte + pos + SAL_PFD), _MM_HINT_T0);
        coordArray[i] = sa_entry;
    }
}

void FMI_search::get_sa_entries(SMEM *smemArray, int64_t *coordArray, int32_t *coordCountArray, uint32_t count, int32_t max_occ)
{
    uint32_t i;
    int32_t totalCoordCount = 0;
    for(i = 0; i < count; i++)
    {
        int32_t c = 0;
        SMEM smem = smemArray[i];
        int64_t hi = smem.k + smem.s;
        int64_t step = (smem.s > max_occ) ? smem.s / max_occ : 1;
        int64_t j;
        for(j = smem.k; (j < hi) && (c < max_occ); j+=step, c++)
        {
            int64_t pos = j;
            int64_t sa_entry = sa_ms_byte[pos];
            sa_entry = sa_entry << 32;
            sa_entry = sa_entry + sa_ls_word[pos];
            //_mm_prefetch((const char *)(sa_ms_byte + pos + SAL_PFD * step), _MM_HINT_T0);
            coordArray[totalCoordCount + c] = sa_entry;
        }
        coordCountArray[i] = c;
        totalCoordCount += c;
    }
}
